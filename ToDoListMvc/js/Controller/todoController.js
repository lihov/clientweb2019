var TODOLIST = TODOLIST || {};

TODOLIST.CONTROLLER = TODOLIST.CONTROLLER || {};

TODOLIST.CONTROLLER.TodoController = function(view, model) {
    this._view = view;
    this._model = model;
    this._init();
};

TODOLIST.CONTROLLER.TodoController.prototype = {
    _init: function() {
        this._view.subscribeToAddItemEvent(this.addTask.bind(this));
        this._view.subscribeToRemoveItemEvent(this.removeTask.bind(this));
        this._view.subscribeToEditItemEvent(this.editTask.bind(this));
    },
    
    addTask: function(sender, args) {
        this._model.addItem(args.description, args.status);
    },

    removeTask: function(sender, args) {
        this._model.removeItem(args.id);
    },

    editTask: function(sender, args) {
        this._model.editItem(args.id, args.value, args.status)
    }
};