var TODOLIST = TODOLIST || {};

TODOLIST.VIEW = TODOLIST.VIEW || {};

TODOLIST.VIEW.TodoView = function(model) {
    this._model = model;

    this._addItemEvent = new TODOLIST.COMMON.Dispatcher(this);
    this._removeItemEvent = new TODOLIST.COMMON.Dispatcher(this);
    this._editItemEvent = new TODOLIST.COMMON.Dispatcher(this);
    this._init();
};

TODOLIST.VIEW.TodoView.prototype = {
    _init: function() {
        this._addButton = document.getElementById("add-button");
        this._taskAddInput = document.getElementById("task-add-input");
        this._rootElement = document.getElementById("tasks-list");

        this._addButton.addEventListener("click", this._addButtonOnClick.bind(this));
        
        this._model.subscribeToModelChangedEvent(this._render.bind(this));
        
        this._render();
    },

    _addButtonOnClick: function() {
        if (this._taskAddInput.value === "") {
            return;
        }

        this._addItemEvent.notify({
            id: TODOLIST.COMMON.getIdUnique(),
            description: this._taskAddInput.value,
            status: "incomplete"
        });
    },

    _removeButtonOnClick: function(sender, args) {
        this._removeItemEvent.notify({
            id: args.id
        });
    },

    _editButtonOnClick: function(sender, args) {
        this._editItemEvent.notify({
            id: args.id,
            value: args.value,
            status: "incomplete"
        });
    },

    _render: function() {
        var rootEl = this._rootElement;
        TODOLIST.COMMON.clearElement(rootEl);
        
        this._mappers = [];

        this._model.getItems().forEach(function(item) {
            var containers = TODOLIST.COMMON.appendListElement(rootEl, item.id, item.description);
            var itemContainer = containers.itemContainer;
            var buttonContainer = containers.buttonContainer;
            
            var mapper = new TODOLIST.VIEW.ItemMapperView(item, itemContainer, buttonContainer);
            mapper.subscribeToRemoveButtonClickItemEvent(this._removeButtonOnClick.bind(this));
            mapper.subscribeToApplyButtonClickItemEvent(this._editButtonOnClick.bind(this));
            this._mappers.push(mapper);
        }.bind(this));
    },

    subscribeToAddItemEvent: function(callback) {
        this._addItemEvent.subscribe(callback);
    },

    subscribeToRemoveItemEvent: function(callback) {
        this._removeItemEvent.subscribe(callback);
    },

    subscribeToEditItemEvent: function(callback) {
        this._editItemEvent.subscribe(callback);
    }
};