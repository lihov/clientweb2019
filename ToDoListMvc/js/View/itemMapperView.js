var TODOLIST = TODOLIST || {};

TODOLIST.VIEW = TODOLIST.VIEW || {};

TODOLIST.VIEW.ItemMapperView = function(item, labelContainer, buttonContainer) {
    this._item = item;
    this._labelContainer = labelContainer;
    this._buttonContainer = buttonContainer;

    this._applyButtonClickItemEvent = new TODOLIST.COMMON.Dispatcher(this);
    this._removeButtonClickItemEvent = new TODOLIST.COMMON.Dispatcher(this);
    
    this._initEditRemove();
};

TODOLIST.VIEW.ItemMapperView.prototype = {
    _initEditRemove: function() {
        this._renderEditRemove();

        this._editButton.addEventListener("click", this._editButtonOnClick.bind(this));
        this._deleteButton.addEventListener("click", this._removeButtonOnClick.bind(this));
    },

    _initApplyCancel: function() {
        this._renderApplyCancel();

        this._applyButton.addEventListener("click", this._applyButtonOnClick.bind(this));
        this._cancelButton.addEventListener("click", this._cancelButtonOnClick.bind(this));
    },

    _renderEditRemove: function() {
        var buttonsEdRem = TODOLIST.COMMON.createEditRemoveButtons(this._buttonContainer);

        this._editButton = buttonsEdRem.edit;
        this._deleteButton = buttonsEdRem.remove;

        TODOLIST.COMMON.createLabel(this._labelContainer, this._item.description);
    },

    _renderApplyCancel: function() {
        var buttonsAppCancel = TODOLIST.COMMON.createApplyCancelButtons(this._buttonContainer);

        this._applyButton = buttonsAppCancel.apply;
        this._cancelButton = buttonsAppCancel.cancel;

        this._edit = TODOLIST.COMMON.createEditInput(this._labelContainer, this._item.description);
    },

    _editButtonOnClick: function() {
        this._initApplyCancel();
    },

    _removeButtonOnClick: function() {
        this._removeButtonClickItemEvent.notify({id: this._item.id});
    },

    _applyButtonOnClick: function() {
        this._applyButtonClickItemEvent.notify({
            id: this._item.id,
            value: this._edit.value,
            status: "incomplete"
        });
        this._initEditRemove();
    },

    _cancelButtonOnClick: function() {
        this._initEditRemove();
    },

    subscribeToApplyButtonClickItemEvent: function(callback) {
        this._applyButtonClickItemEvent.subscribe(callback);
    },

    subscribeToRemoveButtonClickItemEvent: function(callback) {
        this._removeButtonClickItemEvent.subscribe(callback);
    },
};