(function() {
    
    var model = new TODOLIST.MODEL.ItemsList();
    var view = new TODOLIST.VIEW.TodoView(model);
    var controller = new TODOLIST.CONTROLLER.TodoController(view, model);

    function ready() {
        
    }

    document.addEventListener("DOMContentLoaded", ready);
})();