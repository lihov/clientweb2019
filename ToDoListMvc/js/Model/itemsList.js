var TODOLIST = TODOLIST || {};

TODOLIST.MODEL = TODOLIST.MODEL || {};

TODOLIST.MODEL.ItemsList = function() {
    this._items = [];

    this._modelChangedEvent = new TODOLIST.COMMON.Dispatcher(this);
};

TODOLIST.MODEL.ItemsList.prototype = {
    subscribeToModelChangedEvent: function(callback) {
        this._modelChangedEvent.subscribe(callback)
    },

    addItem: function(description, status) {
        this._items.push({
            id: TODOLIST.COMMON.getIdUnique(),
            description: description,
            status: status
        });

        this._modelChangedEvent.notify();
    },

    removeItem: function(id) {
        var itemToDelete = this._items.find(function(item) {
            return item.id === id;
        });

        var index = this._items.indexOf(itemToDelete);

        if (index > -1) {
            this._items.splice(index, 1);
            this._modelChangedEvent.notify();
        }
    },

    editItem: function(id, description, status) {
        var indexToEdit = this._items.findIndex(function(item) {
            return item.id === id;
        });

        if (indexToEdit > -1) {
            this._items[indexToEdit].status = status;
            this._items[indexToEdit].description = description;
            this._modelChangedEvent.notify();
        }
    },

    getItems: function() {
        return this._items;
    }
};