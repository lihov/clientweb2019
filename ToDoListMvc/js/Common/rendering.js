var TODOLIST = TODOLIST || {};

TODOLIST.COMMON = TODOLIST.COMMON || {};

TODOLIST.COMMON.appendListElement = function(rootElement, id) {
    var html = [
        "<div class=\"item-container\">",
        "<div>",
        "</div>",
        "<div class=\"item-button-container\">",
        "</div>",
        "</div>",
    ].join("\n");

    var listElement = document.createElement("li");
    listElement.innerHTML = html;
    listElement.setAttribute("id", id);
    rootElement.appendChild(listElement);

    var itemContainer = listElement.querySelector(".item-container > div");
    var buttonContainer = listElement.querySelector(".item-button-container");

    return {
        itemContainer : itemContainer,
        buttonContainer : buttonContainer
    };
};

TODOLIST.COMMON.clearElement = function(rootElement) {
    rootElement.innerHTML = "";
};

TODOLIST.COMMON.createLabel = function(parentElement, description) {
    parentElement.innerHTML = description;
};

TODOLIST.COMMON.createEditInput = function(parentElement, description) {
    var input = document.createElement("input");
    input.classList.add("input-common");
    input.setAttribute("type", "text");
    input.setAttribute("name", "task-edit-input");
    input.setAttribute("placeholder", "Edit task description");
    input.defaultValue = description;
    
    TODOLIST.COMMON.clearElement(parentElement);

    parentElement.appendChild(input);
    
    return input;
};

TODOLIST.COMMON.createApplyCancelButtons = function(parentElement) {
    var buttonApply = document.createElement("a");
    buttonApply.setAttribute("href", "javascript://");
    buttonApply.innerHTML = "<i class=\"fa fa-check fa-lg\" aria-hidden=\"true\"></i>";
    
    var buttonCancel = document.createElement("a");
    buttonCancel.setAttribute("href", "javascript://");
    buttonCancel.innerHTML = "<i class=\"fa fa-ban fa-lg\" aria-hidden=\"true\"></i>";

    TODOLIST.COMMON.clearElement(parentElement);

    parentElement.appendChild(buttonApply);
    parentElement.appendChild(buttonCancel);

    return {
        apply: buttonApply,
        cancel: buttonCancel
    };
};

TODOLIST.COMMON.createEditRemoveButtons = function(parentElement) {
    var buttonEdit = document.createElement("a");
    buttonEdit.setAttribute("href", "javascript://");
    buttonEdit.innerHTML = "<i class=\"fa fa-pencil fa-lg\" aria-hidden=\"true\"></i>";
    
    var buttonRemove = document.createElement("a");
    buttonRemove.setAttribute("href", "javascript://");
    buttonRemove.innerHTML = "<i class=\"fa fa-minus-circle fa-lg\" aria-hidden=\"true\"></i>";

    TODOLIST.COMMON.clearElement(parentElement);

    parentElement.appendChild(buttonEdit);
    parentElement.appendChild(buttonRemove);

    return {
        edit: buttonEdit,
        remove: buttonRemove
    };
};