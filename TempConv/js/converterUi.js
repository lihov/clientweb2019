var TEMPCONVERTER = TEMPCONVERTER || {};

TEMPCONVERTER.ConverterUi = function() {
    var self = this;
    
    var converter = new TEMPCONVERTER.TempConverter();

    var PRECISION = 2;

    var fahrenheitContainer = document.getElementById("fahrenheit-temperature-value");
    
    var celsiusContainer = document.getElementById("celsius-temperature-value");

    var kelvinContainer = document.getElementById("kelvin-temperature-value");

    var convertButtonFromCe = document.getElementById("convert-button-from-ce");

    var convertButtonFromFa = document.getElementById("convert-button-from-fa");

    var convertButtonFromKe = document.getElementById("convert-button-from-ke");

    var validateAndConvert = function(fromContainer, toContainer, conversionFunc) {
        var sourceValue = fromContainer.value;
        if (sourceValue === "") {
            fromContainer.value = 0;
            fromContainer.select();
            sourceValue = 0;
        }
        
        if (isNaN(sourceValue)) {
            fromContainer.classList.add("input-error");
            return;
        }

        fromContainer.classList.remove("input-error");
        toContainer.value = conversionFunc(parseFloat(sourceValue)).toFixed(PRECISION);
    };

    var convertCelsiusToOthers = function() {
        validateAndConvert(celsiusContainer, fahrenheitContainer, converter.celsiusToFahrenheitConversion);
        validateAndConvert(celsiusContainer, kelvinContainer, converter.celsiusToKelvinConversion);
    };

    var convertFahrenheitToOthers = function() {
        validateAndConvert(fahrenheitContainer, celsiusContainer, converter.fahrenheitToCelsiusConversion);
        validateAndConvert(fahrenheitContainer, kelvinContainer, converter.fahrenheitToKelvinConversion);
    };

    var convertKelvinToOthers = function() {
        validateAndConvert(kelvinContainer, celsiusContainer, converter.kelvinToCelsiusConversion);
        validateAndConvert(kelvinContainer, fahrenheitContainer, converter.kelvinToFahrenheitConversion);
    };

    self.initialize = function() {
        celsiusContainer.addEventListener("input", function(e) {
            convertCelsiusToOthers();
        });

        fahrenheitContainer.addEventListener("input", function(e) {
            convertFahrenheitToOthers();
        });

        kelvinContainer.addEventListener("input", function(e) {
            convertKelvinToOthers();
        });

        convertButtonFromCe.addEventListener("click", function(e) {
            e.preventDefault();
            convertCelsiusToOthers();
        });

        convertButtonFromFa.addEventListener("click", function(e) {
            e.preventDefault();
            convertFahrenheitToOthers();
        });

        convertButtonFromKe.addEventListener("click", function(e) {
            e.preventDefault();
            convertKelvinToOthers();
        });
    };
};