var TEMPCONVERTER = TEMPCONVERTER || {};

TEMPCONVERTER.TempConverter = function() {
    var self = this;

    var C_TO_F_COEFFICIENT = 9 / 5;

    var F_TO_C_COEFFICIENT = 5 / 9;
    
    var FAHRENHEIT_ADDITIVE = 32;

    var KELVIN_ADDITIVE = 273.15;

    self.celsiusToFahrenheitConversion = function(tC) {
        return C_TO_F_COEFFICIENT * tC + FAHRENHEIT_ADDITIVE;
    };

    self.celsiusToKelvinConversion = function(tC) {
        return tC + KELVIN_ADDITIVE;
    };
    
    self.fahrenheitToCelsiusConversion = function(tF) {
        return (tF - FAHRENHEIT_ADDITIVE) * F_TO_C_COEFFICIENT;
    };

    self.fahrenheitToKelvinConversion = function(tF) {
        return self.fahrenheitToCelsiusConversion(tF) + KELVIN_ADDITIVE;
    };

    self.kelvinToCelsiusConversion = function(tK) {
        return tK - KELVIN_ADDITIVE;
    };

    self.kelvinToFahrenheitConversion = function(tK) {
        return self.celsiusToFahrenheitConversion(self.kelvinToCelsiusConversion(tK));
    };
};