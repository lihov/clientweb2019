(function() {
    
    function ready() {
        var ui = new TEMPCONVERTER.ConverterUi();
        ui.initialize();
    }

    document.addEventListener("DOMContentLoaded", ready);
})();