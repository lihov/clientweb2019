var TODOLIST = TODOLIST || {};

TODOLIST.ItemsListView = function(itemsList) {
    this._itemsList = itemsList;
    this._tasksListElement = null;
    this._addButtonElement = null;
    this._addInputElement = null;
    this._init();
};

TODOLIST.ItemsListView.prototype = {
    _itemOnAdd: function(sender) {
        this._render();
    },
    
    _itemOnRemove: function(sender) {
        this._render();
    },
    
    _render: function() {
        var tasksListElement = this._tasksListElement;
        tasksListElement.innerHTML = "";
        this._itemsList.getItems().forEach(function(item) {
            var newItem = new TODOLIST.ItemView(item);
            tasksListElement.appendChild(newItem.getElement());
        });
    },

    _init: function() {
        this._itemsList.onAdd(this._itemOnAdd.bind(this));
        this._itemsList.onRemove(this._itemOnRemove.bind(this));
        
        this._tasksListElement = document.getElementById("tasks-list");
        this._addButtonElement = document.getElementById("add-button");
        this._addInputElement = document.getElementById("task-add-input");

        var itemsList = this._itemsList;
        var addInputElement = this._addInputElement;

        this._addButtonElement.addEventListener("click", function(e) {
            e.preventDefault();
            itemsList.addItem(false, addInputElement.value);
        });
        this._render();
    }
};