var TODOLIST = TODOLIST || {};

TODOLIST.HELPER = TODOLIST.HELPER || {};

TODOLIST.Item = function(status, description) {
    this._status = status;
    this._description = description;
    this._id = TODOLIST.HELPER.getIdUnique();
    this._editItemEvent = new TODOLIST.Dispatcher(this);
    this._deleteItemEvent = new TODOLIST.Dispatcher(this);
};

TODOLIST.Item.prototype = {
    edit: function(newDescription) {
        this._description = newDescription;
        this._editItemEvent.notify(this._description);
    },
    
    delete: function() {
        this._deleteItemEvent.notify();
    },

    getDescription: function() {
        return this._description;
    },

    getStatus: function() {
        return this._status;
    },

    getId: function() {
        return this._id; 
    },

    onEdit: function(callback) {
        this._editItemEvent.subscribe(callback);
    },

    onDelete: function(callback) {
        this._deleteItemEvent.subscribe(callback);
    }
};
