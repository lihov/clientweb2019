var TODOLIST = TODOLIST || {};

TODOLIST.ItemsList = function() {
    this._items = [];
    this._addItemEvent = new TODOLIST.Dispatcher(this);
    this._removeItemEvent = new TODOLIST.Dispatcher(this);
};

TODOLIST.ItemsList.prototype = {
    
    addItem : function(item) {
        this._items.push(item);
        console.log(this._items);
        this._addItemEvent.notify();
    },

    removeItem : function(taskId) {
        var toDelete = this._items.find(function(item) {
            return item.getId() === taskId;
        });

        var index = this._items.indexOf(toDelete);

        if (index > -1) {
            this._items.splice(index, 1);
            this._removeItemEvent.notify();
        }
    },

    getItems : function() {
        return this._items.splice(0);
    },

    onAdd: function(callback) {
        this._addItemEvent.subscribe(callback);
    },

    onRemove: function(callback) {
        this._removeItemEvent.subscribe(callback);
    }

};