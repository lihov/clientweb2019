var TODOLIST = TODOLIST || {};

TODOLIST.ItemView = function(item) {
    this._item = item;
    this._rootElement = null;
    this._init();
};

TODOLIST.ItemView.prototype = {
    _getItemContent: function() {
        return [
            "<div class=\"item-container\">",
            "<div>",
            this._item.getDescription(),
            "</div>",
            "<div class=\"item-button-container\">",
            "<a href=\"javascript://\">",
            "<i class=\"fa fa-pencil-square-o fa-lg\" aria-hidden=\"true\"></i>",
            "</a>",
            "<a href=\"javascript://\">",
            "<i class=\"fa fa-minus-circle fa-lg\" aria-hidden=\"true\"></i>",
            "</a>",
            "</div>",
            "</div>",
        ].join("\n");
    },
    
    _itemOnEdit: function(sender, eventArgs) {

    },
    
    _itemOnDelete: function(sender, eventArgs) {

    },

    // _init: function() {
    //     var parentContainer = document.querySelector(".todo-list");
    //     parentContainer.appendChild(_getItemContent());

    //     this._buttonEditElement = document.querySelector(
    //         "#" + this._item.getId() + " a:nth-child(1)");

    //     this._buttonDeleteElement = document.querySelector(
    //         "#" + this._item.getId() + " a:nth-child(2)");
        
    //     this._item.onEdit(this._itemOnEdit);
    // },

    _init: function() {
        this._item.onEdit(this._itemOnEdit.bind(this));
        this._item.onDelete(this._itemOnDelete.bind(this));
        
        this._rootElement = document.createElement("li");
        this._rootElement.innerHTML = this._getItemContent();
        this._rootElement.setAttribute("id", this._item.getId());
    },

    getElement: function() {
        return this._rootElement;
    }
};