var TODOLIST = TODOLIST || {};

TODOLIST.Dispatcher = function(sender) {
    this._sender = sender;
    this._subscribers = [];
};

TODOLIST.Dispatcher.prototype = {
    subscribe: function(subscriber) {
        this._subscribers.push(subscriber);
    },

    notify: function(eventArgs) {
        this._subscribers.forEach(function(subscriber) {
            subscriber(this._sender, eventArgs);
        });
    }
};