var TODOLIST = TODOLIST || {};

TODOLIST.HELPER = TODOLIST.HELPER || {};

TODOLIST.HELPER.getIdUnique = function() {
    return "_" + Math.random().toString(36).substr(2, 9);
};