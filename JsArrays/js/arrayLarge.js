var ARRAYS = ARRAYS || {};

ARRAYS.ArrayLarge = function() {
    var self = this;

    var ARRAY_UP_BOUND = 100;

    var DENOMINATOR = 2;

    var initArray = function() {
        return Array.apply(null, {length: ARRAY_UP_BOUND}).map(function(value, index) {
            return index + 1;
        });
    };

    self.calculateEvenSquare = function() {
        var array = initArray();
        var evenArray = array.filter(function (item) {
            return item % DENOMINATOR === 0;
        });

        return evenArray.map(function(item) {
            return item * item;
        });
    };
    
    self.printBaseArray = function() {
        console.log("Base array " + initArray());
    };

    self.printEvenSquare = function() {
        console.log("Even square " + self.calculateEvenSquare());
    };
};