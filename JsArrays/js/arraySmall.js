var ARRAYS = ARRAYS || {};

ARRAYS.ArraySmall = function() {
    var self = this;
    
    var SLICE_NUMBER = 5;

    var DENOMINATOR = 2;

    var initArray = function() {
        return [4, 7, 9, 5, 4, 7, 8];
    };

    self.getDescending = function() {
        return initArray().sort(function(a, b) {
            return b - a;
        });
    };

    self.getFirstElements = function(number) {
        var array = initArray();
        var endIndex = Math.min(array.length, number);
        return array.slice(0, endIndex);
    };

    self.getLastElements = function(number) {
        var array = initArray();
        var startIndex = Math.max(array.length - number, 0);
        return array.slice(startIndex);
    };

    self.sumEven = function() {
        var array = initArray();       
        return array.reduce(function(accumulator, a) {
            return a % DENOMINATOR === 0 ? accumulator + a : accumulator;
        }, 0);
    };

    self.printBaseArray = function() {
        console.log("Base Array " + initArray());
    };

    self.printDescending = function() {
        console.log("Descending " + self.getDescending());
    };

    self.printFirstElements = function() {
        console.log("First " + SLICE_NUMBER + " elements " + self.getFirstElements(SLICE_NUMBER));
    };

    self.printLastElements = function() {
        console.log("Last " + SLICE_NUMBER + " elements " + self.getLastElements(SLICE_NUMBER));
    };

    self.printSumEven = function() {
        console.log("Sum even " + self.sumEven());
    };
};
