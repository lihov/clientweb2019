(function() {

    var arr = new ARRAYS.ArraySmall();
    arr.printBaseArray();
    arr.printDescending();
    arr.printFirstElements();
    arr.printLastElements();
    arr.printSumEven();

    var arrSq = new ARRAYS.ArrayLarge();
    arrSq.printBaseArray();
    arrSq.printEvenSquare();

    var cnt = new COUNTRIES.CountriesList();
    cnt.printCountries();
    cnt.printMaxCitiesNumber();
    cnt.printPopulation();
    cnt.printPopulationObject();

})();