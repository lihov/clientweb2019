var COUNTRIES = COUNTRIES || {};

COUNTRIES.CountriesList = function() {
    var self = this;

    var countryStorage = [
        {
            name : "Germany",
            cities : [
                {
                    name : "Berlin", 
                    population : 1000000
                }, 
                {
                    name : "Bremen", 
                    population : 800000
                }, 
                {
                    name : "Koln", 
                    population : 700000
                }, 
                {
                    name : "Bonn", 
                    population : 870000
                }
            ]
        },
        {
            name : "France",
            cities : [
                {
                    name : "Paris", 
                    population : 1200000
                }, 
                {
                    name : "Lion", 
                    population : 850000
                }, 
                {
                    name : "Marseilles", 
                    population : 620000
                }
            ]
        },
        {
            name : "Spain",
            cities : [
                {
                    name : "Madrid", 
                    population : 1100000
                }, 
                {
                    name : "Barcelona", 
                    population : 570000
                }, 
                {
                    name : "Valencia", 
                    population : 650000
                }, 
                {
                    name : "Alicante", 
                    population: 100000
                }
            ]
        }
    ];

    self.getMaxCitiesNumber = function() {
        var maxCount = Math.max.apply(Math, countryStorage.map(function(c) {
            return c.cities.length;
        }));

        return countryStorage.filter(function(c) {
            return c.cities.length === maxCount;
        });
    };

    self.getPopulation = function() {
        return countryStorage.map(function(c) {
            return { 
                name : c.name, 
                population : c.cities.reduce(function(sum, current) {
                    return sum + current.population;
                }, 0)
            };
        });
    };
    
    self.getPopulationObject = function() {
        return countryStorage.reduce(function(obj, cursor) {
            return Object.assign(obj, {[cursor.name] : cursor.cities.reduce(function(sum, current) {
                return sum + current.population;
            }, 0)});
        }, {});
    };

    self.printCountries = function() {
        console.log("Countries: ", countryStorage);
    };

    self.printMaxCitiesNumber = function() {
        console.log("Max number of cities: ", self.getMaxCitiesNumber());
    };

    self.printPopulation = function() {
        console.log("Population: ", self.getPopulation());
    };

    self.printPopulationObject = function() {
        console.log("Population object: ", self.getPopulationObject());
    };
};