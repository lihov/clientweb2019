var TODOLIST = TODOLIST || {};

TODOLIST.COMMON = TODOLIST.COMMON || {};

TODOLIST.COMMON.Dispatcher = function(sender) {
    this._sender = sender;
    this._subscribers = [];
};

TODOLIST.COMMON.Dispatcher.prototype = {
    subscribe: function(subscriber) {
        this._subscribers.push(subscriber);
    },

    notify: function(eventArgs) {
        this._subscribers.forEach(function(subscriber) {
            subscriber(this._sender, eventArgs);
        });
    }
};