var TODOLIST = TODOLIST || {};

TODOLIST.COMMON = TODOLIST.COMMON || {};

TODOLIST.COMMON.createListElement = function(rootElement, item) {
    var html = [
        "<div class=\"item-container\">",
        "<div>",
        item.description,
        "</div>",
        "<div class=\"item-button-container\">",
        "</div>",
        "</div>",
    ].join("\n");

    var listElement = document.createElement("li");
    listElement.innerHTML = html;
    listElement.setAttribute("id", item.id);
    rootElement.appendChild(listElement);

    return listElement;
};