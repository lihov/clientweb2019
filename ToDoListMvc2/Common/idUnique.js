var TODOLIST = TODOLIST || {};

TODOLIST.COMMON = TODOLIST.COMMON || {};

TODOLIST.COMMON.getIdUnique = function() {
    return "_" + Math.random().toString(36).substr(2, 9);
};