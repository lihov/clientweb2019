var TODOLIST = TODOLIST || {};

TODOLIST.VIEW = TODOLIST.VIEW || {};

TODOLIST.VIEW.ItemView = function(item, rootElement) {
    this._item = item;
    this._rootElement = rootElement;

    this._editButtonClickEvent = new TODOLIST.COMMON.Dispatcher(this);
    this._deleteButtonClickEvent = new TODOLIST.COMMON.Dispatcher(this);
};

TODOLIST.VIEW.ItemView.prototype = {
    _init: function() {
        listElement = TODOLIST.COMMON.createListElement(this._rootElement, this._item);

        
    },
};